<?php


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login Page';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to login:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['class' => 'form-horizontal']); ?>

            <?= $form->field($model_login, 'email')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model_login, 'password')?>

            <div style="color:#999;margin:1em 0">
                If you forgot your password you can <?= Html::a('reset it', ['Register/request-password-reset']) ?>.
            </div>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>