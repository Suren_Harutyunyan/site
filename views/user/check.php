<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Registration Page';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to Registration:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['class' => 'form-horizontal', 'options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->field($model, 'username')->textInput() ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password') ?>

            <?= $form->field($model, 'file')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

            <div style="color:#999;margin:1em 0">
                If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
            </div>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

