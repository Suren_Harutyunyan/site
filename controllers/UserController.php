<?php


namespace frontend\controllers;

use frontend\models\Checking;
use frontend\models\Login;
use frontend\models\Users;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\UploadedFile;


class UserController extends Controller
{
    public $file;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['home'],
                'rules' => [

                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

        ];
    }

    public function actionHome()
    {
        return $this->render('home');
    }

    public function actionCheck()
    {
        $model = new Checking();

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file !== null) {
                $model->file->saveAs('photo/' . $model->file->baseName . "." . $model->file->extension);
            }
            if ($model->registration()) {

                return $this->render('home');
            }
        }

        return $this->render('check', ['model' => $model]);
    }

    public function actionLogin()
    {

        $model_login = new Login();
        if ($model_login->load(\Yii::$app->request->post()) && $model_login->logine()) {

            return $this->render('home');
        }


        return $this->render('login', ['model_login' => $model_login]);
    }

    /*public function actionUpdate($id)
    {

        $modelUpdate = new Users($id);

        if ($modelUpdate->load(\Yii::$app->request->post()) && $modelUpdate->save()) {
            return $this->redirect(['view', 'id' => $modelUpdate->id]);
        }
        return $this->render('update', ['modelUpdate' => $modelUpdate]);

    }*/
}