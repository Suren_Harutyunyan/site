<?php


namespace frontend\models;

use yii\base\Model;

class Login extends Model
{
    public $email;

    public $password;

    public function rules()
    {
        return [

            [['email', 'password'], 'required'],
            ['email', 'email'],
            ['password', 'validatePassword'],

        ];
    }

    public function validatePassword($attribute)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'password or a email entered incorrectly');
            }
        }
    }

    public function getUser()
    {
        return Users::findOne(['email' => $this->email]);
    }

    public function logine()
    {
        if ($this->validate()) {
            return \Yii::$app->user->login($this->getUser());

        } else {
            return false;
        }
    }
}