<?php

namespace frontend\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class Users extends ActiveRecord implements IdentityInterface
{

    public static function tableName()
    {
        return 'users';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function setPassword($password)
    {
        $salt = sha1($password);
        $salt1 = md5('c4a');
        $this->password = sha1($password) . $salt . sha1($password) . $salt1 . md5($password);
    }

    public function validatePassword($password)
    {
        $salt = sha1($password);
        $salt1 = md5('c4a');
        return $this->password === sha1($password) . $salt . sha1($password) . $salt1 . md5($password);
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public function getId()
    {
        return $this->id;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {

    }

    public function getAuthKey()
    {

    }

    public function validateAuthKey($authKey)
    {

    }
}

