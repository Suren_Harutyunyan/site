<?php


namespace frontend\models;

use yii\base\Model;


class Checking extends Model
{
    public $username;

    public $email;

    public $password;

    public $file;

    public function rules()
    {
        return [
            [['username', 'email', 'password'], 'default'],
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u', 'message' => 'unacceptable spelling (a space between the letters should not be)'],
            ['username', 'unique', 'targetClass' => 'frontend\models\Users'],
            ['username', 'string', 'min' => 3, 'max' => 10],

            [['email', 'password'], 'required'],
            ['email', 'email'],
            ['email', 'string', 'min' => 5, 'max' => 30],
            [['email'], 'trim'],
            ['email', 'unique', 'targetClass' => 'frontend\models\Users'],

            [['password'], 'trim'],
            ['password', 'string', 'min' => 5, 'max' => 15],
            ['password', 'match', 'pattern' => '/^[\*a-zA-Z0-9]{5,15}$/', 'message' => 'unacceptable spelling (a space between the letters should not be)'],

            [['file'], 'file', 'extensions' => 'jpeg, jpg, png', /*'maxFiles' => 3*/]

        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'email' => 'Email Address',
            'password' => 'Password',
            'file' => 'Photo',
        ];
    }

    public function registration()
    {
        $user = new Users();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->file = $this->file;

        return $user->save() ? $user : null;
    }


}